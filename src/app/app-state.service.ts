import {Injectable, OnInit} from '@angular/core';
import {Router, Routes} from "@angular/router";
import {MobileHomeComponent} from "./modules/mobile/mobile-home/mobile-home.component";
import {MobileBoxComponent} from "./modules/mobile/mobile-box/mobile-box.component";
import {PrintComponent} from "./modules/desktop/print/print.component";
import {BoxComponent} from "./modules/desktop/box/box.component";
import {HomeComponent} from "./modules/desktop/home/home.component";

@Injectable({
    providedIn: 'root'
})
export class AppStateService implements OnInit {
    public static appName: string = 'New box order';
    private router: Router;
    private isMobileVersion = false;
    private mobileRoutes: Routes  = [
        {path: '', component: MobileHomeComponent},
        {path: 'box', component: MobileBoxComponent},
        {path: 'box/:id', component: MobileBoxComponent},
        {path: 'print', component: PrintComponent},
        {path: '**', redirectTo: ''}
    ];
    private desktopRoutes: Routes  = [
        {path: '', component: HomeComponent},
        {path: 'box', component: BoxComponent},
        {path: 'box/:id', component: BoxComponent},
        {path: 'print', component: PrintComponent},
        {path: '**', redirectTo: ''}
    ]

    constructor(router: Router) {
        this.router = router;
    }

    ngOnInit(): void {
        this.isMobile();
    }

    public isMobile(): boolean {
        let currentState: boolean = this.isMobileVersion;
        if (window.innerWidth < 768) {
            this.isMobileVersion = true;
        } else {
            this.isMobileVersion = false;
        }
        this.onChangeState(currentState)

        return this.isMobileVersion;
    }

    private onChangeState(state: boolean): void
    {
        if (state == this.isMobileVersion) {
            return;
        }
        if (this.isMobileVersion)  {
            this.router.resetConfig(this.mobileRoutes)
        } else
        {
            this.router.resetConfig(this.desktopRoutes);
        }
        this.router.navigateByUrl('/');
    }
}
