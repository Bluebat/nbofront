import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
    providedIn: 'root'
})
export class ApiClientService {

    private apiUrl: string = 'http://192.168.0.165:8002/api/v1/';

    constructor(private client: HttpClient) {
    }

    public getBoxes()
    {
        return this.client.get(this.apiUrl + 'boxes');
    }

    public getBox(id)
    {
        return this.client.get(this.apiUrl + 'boxes/' + id);
    }

    public getLocations()
    {
        return this.client.get(this.apiUrl + 'locations');
    }

    public getBarcodes() {
        return this.client.get(this.apiUrl + 'barcodes');
    }

    public sendRequest(param: { images: string[]; form: any }): object {
        this.client.post(this.apiUrl + 'boxes', param)
            .subscribe(
                response => {

                    return response;
                },
                error => {
                    console.log(error.error);

                    return error.error;
                }
            )
        return {};
    }
}
