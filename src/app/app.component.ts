import {Component, OnInit} from '@angular/core';
import {AppStateService} from "./app-state.service";
import {Router} from "@angular/router";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {

    private isMobile = false;

    constructor(private router: Router, private appState: AppStateService) {
    }

    ngOnInit() {
        this.setIsMobile();
    }

    onResize() {
        this.setIsMobile();
    }

    private setIsMobile() {
        this.isMobile = this.appState.isMobile();
    }
}
