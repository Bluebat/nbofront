import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from "./app-routing.module";
import {HttpClientModule} from "@angular/common/http";
import { ReactiveFormsModule} from "@angular/forms";

import {AppComponent} from './app.component';
import {NavComponent} from './modules/desktop/nav/nav.component';
import {HomeComponent} from './modules/desktop/home/home.component';
import {BoxComponent} from './modules/desktop/box/box.component';
import {PrintComponent} from "./modules/desktop/print/print.component";
import { MobileNavComponent } from './modules/mobile/mobile-nav/mobile-nav.component';
import { MobileHomeComponent } from './modules/mobile/mobile-home/mobile-home.component';
import { MobileBoxComponent } from './modules/mobile/mobile-box/mobile-box.component';


@NgModule({
    declarations: [
        AppComponent,
        NavComponent,
        HomeComponent,
        BoxComponent,
        PrintComponent,
        MobileNavComponent,
        MobileHomeComponent,
        MobileBoxComponent
    ],
    entryComponents: [
        AppComponent,
        NavComponent,
        HomeComponent,
        BoxComponent,
        PrintComponent,
        MobileNavComponent,
        MobileHomeComponent,
        MobileBoxComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        ReactiveFormsModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})

export class AppModule {
}
