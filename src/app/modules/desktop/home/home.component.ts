import {Component, OnInit} from '@angular/core';
import {ApiClientService} from "../../../api-client.service";

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    boxes: Object;

    constructor(private api: ApiClientService) {
    }

    ngOnInit() {
        this.api.getBoxes().subscribe(response => {
           this.boxes = response;
           console.log(response);
        });
    }

}
