import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
    selector: 'app-box',
    templateUrl: './box.component.html',
    styleUrls: ['./box.component.scss']
})
export class BoxComponent implements OnInit {

    boxForm: FormGroup;
    submitted = false;
    success = false;

    constructor(private formBuilder: FormBuilder) {
        this.boxForm = this.formBuilder.group({
            boxName: ['', Validators.required],
            boxDescription: ['', Validators.required]
        });
    }

    ngOnInit() {
    }

    onSubmit() {
        this.submitted = true;
        if (this.boxForm.invalid) {
            return;
        }

        this.success = true;
    }
}
