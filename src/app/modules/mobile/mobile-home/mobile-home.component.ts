import { Component, OnInit } from '@angular/core';
import {ApiClientService} from "../../../api-client.service";

@Component({
  selector: 'app-mobile-home',
  templateUrl: './mobile-home.component.html',
  styleUrls: ['./mobile-home.component.scss']
})
export class MobileHomeComponent implements OnInit {

  boxes: Object;

  constructor(private api: ApiClientService) {
  }

  ngOnInit() {
    this.api.getBoxes().subscribe(response => {
      this.boxes = response;
    });
  }

}
