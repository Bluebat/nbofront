import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ApiClientService} from "../../../api-client.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
    selector: 'app-mobile-box',
    templateUrl: './mobile-box.component.html',
    styleUrls: ['./mobile-box.component.scss']
})
export class MobileBoxComponent implements OnInit {

    /**
     * Form Group
     */
    private boxForm: FormGroup;

    /**
     * Locations
     */
    private locations: object;

    /**
     * Barcodes
     */
    private barcodes: object;

    /**
     * Api response
     */
    private apiResponse: object;

    /**
     * Create or edit
     */
    private isNewBox: boolean = true;

    /**
     * Box object
     */
    private box: object;

    /**
     * Array contains form images names
     */
    private imageNames: string[] = ['image1'];

    /**
     * Array contains form images temporary path
     */
    private imagePaths: string[] = [''];

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private api: ApiClientService,
        private formBuilder: FormBuilder
    ) {
        this.boxForm = this.formBuilder.group({
            boxName: ['', Validators.required],
            boxDescription: ['', Validators.required],
            boxParent: [''],
            boxLocations: ['']
        });
    }

    public ngOnInit(): void
    {
        let id: number = 0;
        this.route.paramMap.subscribe(params => {
            id = parseInt(params.get('id'));
        });
        if (id > 0) {
            this.isNewBox = false;
            this.api.getBox(id).subscribe(response => {
                this.box = response;
            });
        }
        this.api.getLocations().subscribe(response => {
            this.locations = response;
        });
        this.api.getBarcodes().subscribe(response => {
            this.barcodes = response;
        });
    }

    private onSubmit(): void
    {
        this.apiResponse = this.api.sendRequest({'form': this.boxForm.value, 'images': this.imagePaths});
    }

    private addInputImage(): void
    {
        this.imageNames.push('image' + (this.imageNames.length + 1));
        this.imagePaths.push('');
    }

    private deleteInputImage(): void
    {
        this.imageNames.pop();
        this.imagePaths.pop();
    }

    /**
     * to do handle promise
     */
    private back(): void
    {
        this.router.navigate(['/']);
    }

    private preview(image: any, index: number): void
    {
        if (image.length === 0) {
            return;
        }
        let reader = new FileReader();
        reader.readAsDataURL(image[0]);
        reader.onload = () => {
            if (typeof reader.result === 'string') {
                this.imagePaths[index] = reader.result;
            }
        }
    }
}
