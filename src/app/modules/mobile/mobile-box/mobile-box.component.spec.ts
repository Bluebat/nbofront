import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MobileBoxComponent } from './mobile-box.component';

describe('MobileBoxComponent', () => {
  let component: MobileBoxComponent;
  let fixture: ComponentFixture<MobileBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MobileBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
