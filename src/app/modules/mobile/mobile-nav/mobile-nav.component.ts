import { Component, OnInit } from '@angular/core';
import { AppStateService } from "../../../app-state.service";
import {FormGroup} from "@angular/forms";

@Component({
  selector: 'app-mobile-nav',
  templateUrl: './mobile-nav.component.html',
  styleUrls: ['./mobile-nav.component.scss']
})
export class MobileNavComponent implements OnInit {

  constructor() { }

  private appName: string = AppStateService.appName;
  private searchForm: object = new FormGroup({});

  ngOnInit() {
  }

}
